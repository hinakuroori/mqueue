#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#define NR_THREAD_MAX 1024
static struct mq_info {
	mqd_t rx;
	mqd_t tx;
	char name[128];
} mq_info[NR_THREAD_MAX];

static int nt = 1;
static long maxmsg = 10;
static long msgsize = 8192;
static long bufsize = 8192;
static long txcount = 10;

static void dump_mq_state(const char *pre, const char *name, const mqd_t mq)
{
	struct mq_attr attr = {0};
	if (0 != mq_getattr(mq, &attr)) {
		perror("mq_getattr:");
		exit(-1);
	}
	printf("[%s]%s:flags=0x%lx, maxmsg=%ld, msgsize=%ld, curmsgs=%ld\n",
		pre, name, attr.mq_flags, attr.mq_maxmsg,
		attr.mq_msgsize, attr.mq_curmsgs);
	return;
}

static void create_tx_mqueue(int idx)
{
	mqd_t ret = -1;
	sprintf(mq_info[idx].name, "/testmq%d", idx);

	ret = mq_open(mq_info[idx].name, O_WRONLY|O_CLOEXEC|O_NONBLOCK);
	if (ret == -1) {
		perror("mq_open:");
		exit(-1);
	} else {
		dump_mq_state("TX", mq_info[idx].name, ret);
	}
	mq_info[idx].tx = ret;
}

static void create_rx_mqueue(int idx, long maxmsg, long msgsize)
{
	mqd_t ret = -1;
	struct mq_attr attr = {0};

	sprintf(mq_info[idx].name, "/testmq%d", idx);
	attr.mq_maxmsg = maxmsg;
	attr.mq_msgsize = msgsize;

	ret = mq_open(mq_info[idx].name,
			O_RDONLY|O_CLOEXEC|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO,
			&attr);
	if (ret == -1) {
		perror("mq_open:");
		exit(-1);
	} else {
		dump_mq_state("RX", mq_info[idx].name, ret);
	}
	mq_info[idx].rx = ret;
}

static void dump_proc(void) 
{
	FILE *fp = NULL;
	char tmp[32] = {0};

	memset(tmp, 0, sizeof(tmp));
	fp = fopen("/proc/sys/fs/mqueue/msg_default", "r");
	if (fp == NULL) {
		perror("fopen(msg_default):");
		exit(-1);
	}
	fread(tmp, sizeof(char), (sizeof(tmp)/sizeof(tmp[0])), fp);
	printf("msg_default:%s", tmp);
	fclose(fp);

	memset(tmp, 0, sizeof(tmp));
	fp = fopen("/proc/sys/fs/mqueue/msg_max", "r");
	if (fp == NULL) {
		perror("fopen(msg_max):");
		exit(-1);
	}
	fread(tmp, sizeof(char), (sizeof(tmp)/sizeof(tmp[0])), fp);
	printf("msg_max:%s", tmp);
	printf("HARD_MAX:%u\n", 65536);
	printf("HARD_MSGSIZEMAX:%u\n", 16777216);
	fclose(fp);

	memset(tmp, 0, sizeof(tmp));
	fp = fopen("/proc/sys/fs/mqueue/msgsize_default", "r");
	if (fp == NULL) {
		perror("fopen(msgsize_default):");
		exit(-1);
	}
	fread(tmp, sizeof(char), (sizeof(tmp)/sizeof(tmp[0])), fp);
	printf("msgsize_default:%s", tmp);
	fclose(fp);

	memset(tmp, 0, sizeof(tmp));
	fp = fopen("/proc/sys/fs/mqueue/msgsize_max", "r");
	if (fp == NULL) {
		perror("fopen(msgsize_max):");
		exit(-1);
	}
	fread(tmp, sizeof(char), (sizeof(tmp)/sizeof(tmp[0])), fp);
	printf("msgsize_max:%s", tmp);
	fclose(fp);

	memset(tmp, 0, sizeof(tmp));
	fp = fopen("/proc/sys/fs/mqueue/queues_max", "r");
	if (fp == NULL) {
		perror("fopen(queues_max):");
		exit(-1);
	}
	fread(tmp, sizeof(char), (sizeof(tmp)/sizeof(tmp[0])), fp);
	printf("queues_max:%s", tmp);
	fclose(fp);
}

static void test_tx(int idx)
{
	void *p = malloc(bufsize);
	if (p == NULL) {
		perror("malloc:");
		exit(-1);
	}

	if (0 == mq_send(mq_info[idx].tx, p, bufsize, 0)) {
		printf("[%d]mq_send(%ld):succeeded\n", idx, bufsize);
	} else {
		perror("mq_send:");
	}
	free(p);
	p = NULL;
}

int main(int argc, char *argv[])
{
	int i = 0;
	int j = 0;

	if (argc >= 6) {
		nt = atoi(argv[1]);
		maxmsg = (long)atoi(argv[2]);
		msgsize = (long)atoi(argv[3]);
		bufsize = (long)atoi(argv[4]);
		txcount = (long)atoi(argv[5]);
		printf("nthr:%d msgmax:%ld msgsize:%ld\n", nt, maxmsg, msgsize);
		printf("bufsize:%ld txcount:%ld\n", bufsize, txcount);
	}

	dump_proc(); 

	for (i = 0; i < nt; i++) {
		create_rx_mqueue(i, maxmsg, msgsize);
		create_tx_mqueue(i);
	}

	for (i = 0; i < nt; i++) {
		for (j = 0; j < txcount; j++) {
			test_tx(i);
		}
	}
}
